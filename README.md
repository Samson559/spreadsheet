This is a basic spreadsheet capable of computing simple mathematical formulas involving spreadsheet cells.
Cells are stored and updated via a dependency graph. Formulas are evaluated by parsing text values with regular expressions to make sense of their mathematical
symbols, then updating the GUI to reflect changes in the dependency graph. Spreadsheets can be saved/opened in xml formatted files. The spreadsheet obeys the MVC model
by separating logic and user interface code.